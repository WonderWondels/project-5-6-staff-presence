package relational;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import main.Database;

public class RelationalDB implements Database{
	
	private Connection con;
	private Boolean connected = false;
	
	public RelationalDB() {
		try {

			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection(
				"jdbc:mysql://localhost:3306/sys",
				"expvm",
				"test"
				);
			
			connected = true;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public int approach(int uid) {
		
		try {
			
			// Inserts a new approach into the database.
			String query = "INSERT INTO sys.checks (uid) VALUES('" + uid + "')";
			Statement st = con.createStatement();
			st.executeUpdate(query);
			
			// Counts how many times person has approached in the past to determine wether they are checking in or out.
			query = "SELECT COUNT(uid) as count FROM sys.checks WHERE uid = '" + uid + "'";
			st = con.createStatement();
			ResultSet rs = st.executeQuery(query);
			
			// Returns wether this approach was used to check in or check out.
			if (!rs.next()) {
				// If nothing was found this is the first entry so always a check in.
				return CHECKED_IN;
			} else if ((rs.getInt("count") % 2) == 0) {
				// If amount of times approached is even the person is checking in.
				return CHECKED_IN;
			} else {
				// If amount of times approached is uneven this is a checkout.
				return CHECKED_OUT;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// At this point an exception was caught, server error lets the user know something is off.
		return SERVER_ERROR;
		
	}
	
	@Override
	public void clear() {
		try {
			// Clears the table from any contents.
			String query = "TRUNCATE sys.checks";
			Statement st = con.createStatement();
			st.executeUpdate(query);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public Boolean isConnected() {
		// Returns true if connection was established in the constructor.
		return connected;
	}

	@Override
	public List<String> getData(String file) {
		List<String> list = new ArrayList<String>();
		ResultSet rs;
		
		try {
			String query = "SELECT value FROM value";
			Statement st = con.createStatement();
			rs = st.executeQuery(query);
			
			while(rs.next()) {
				list.add(rs.getString(rs.findColumn("value")));
			}	
		} catch(Exception e) {
			
		}
		
		return list;
	}

	@Override
	public void writeData(List<String> list, String file) {
		for(int i = 0; i < list.size(); i++) {
			try {
				String query = "INSERT INTO value(value) VALUES('" + list.get(i) + "')";
				Statement st = con.createStatement();
				st.executeUpdate(query);
				
					
			} catch(Exception e) {
				e.printStackTrace();
				System.out.println("I failed you master.");
			}
		}
	}

	@Override
	public int newEmployee(String employee) {
		//naam, functie, afdeling, bhv, ehbo, uid
		try {
			String query = "INSERT INTO sys.employee (name,functie,afdeling,bhv,ehbo,uid) VALUES("+employee+")";
			System.out.println(query);
			Statement st = con.createStatement();
			st.executeUpdate(query);
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
		return 0;
	}
}
