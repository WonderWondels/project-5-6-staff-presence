package main;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import relational.*;
import flatfile.*;

public class Main {
	
	private static Scanner sc;

	public static void main(String[] args) {
		
		// Starting scanner and letting the user decide what database they would like to use.
		System.out.println("Hello World.");
		sc = new Scanner(System.in);
		System.out.println("-+- Select Data Management System: -+-");
		String format = "%-20s %5d\n";
		System.out.format(format, "Relational", 0);
		System.out.format(format, "Object", 1);
		System.out.format(format, "FlatFile", 2);
		System.out.format(format, "Xibo", 3);
		int input = waitForInput(sc);
		
		// Contains a switch case deciding which database to use.
		Database database = getDB(input);
		
		// System keeps running intil shut down by user input.
		while(true) {
			// Stops the cycle if connection to database has not been established.
			if(!database.isConnected()) { 
				System.out.println("Connection could not be established.");
				break; 
			}
			
			// Ask the user what task they would like performed.
			System.out.println("-+- What would you like to do?: -+-");
			format = "%-20s %5d\n";
			System.out.format(format, "Approach once plz", 0);
			System.out.format(format, "Clear db plz", 1);
			System.out.format(format, "Timed", 2);
			System.out.format(format, "Experiment 1", 2);
			System.out.format(format, "Just approach this amount of times", 9);
			System.out.format(format, "Stop", -1);
			
			// Waits for input from user.
			input = waitForInput(sc);
			if(input == -1) {
				sc.close();
				break;
			}
			switch(input) {
			case 0:
				// Approaches the database once.
				database.approach(1);
				break;
			case 1:
				// Clears the database.
				database.clear();
				break;
			case 2:
				// Starts a timed cycle.
				System.out.println("Amount of times to sample: ");
				input = waitForInput(sc);
				timedCycle(database, input);
				break;
			case 3:
				List<String> list = new ArrayList<String>();
				fillWithData(list, 500);
				
				// Writes and then gets the data from the server.
				// Weird parameters.. I know, read the comments in the Database class.
				database.writeData(list, "test.txt");
				List<String> list2 = database.getData("test.txt");
				
				// Compares the two lists.
				if(list.equals(list2)) {
					System.out.println("The two lists were exactly the same great job.");
				} else {
					System.out.println("The two lists were different something went wrong.");
				}
				break;
			default:
				// Simple thing for testing, any number not in the switch case will lead to the program calling
				// the approach method of the databases. This was easy for testing.
				for(int i = 0; i < input; i++) {
					System.out.println(database.approach(1));
				}
				break;
			}
		}
	}
	
	// Simple switch case to let the user chose the database they would like.
	private static Database getDB(int input) {
		Database database;
		switch(input) {
		case 0:
			database = new RelationalDB();
			return database;
		case 1:
			System.out.println("This database type is not working yet, please select another.");
			return getDB(waitForInput(sc));
		case 2:
			database = new FlatFileDB();
			return database;
		case 3:
			System.out.println("This database type is not working yet, please select another.");
			return getDB(waitForInput(sc));
		}
		
		
		return null;
	}
	
	// Waits for the users input then returns it.
	private static int waitForInput(Scanner sc) {
		while(!sc.hasNext()) {}
		int input = sc.nextInt();
		return input;
	}
	
	private static void timedCycle(Database database, int amount) {
		// 2 longs for time, beginTime later used to determine how long the entire process took
		// and totalTimeElapsed for the time the pushing to the database took on its own.
		long beginTime = System.nanoTime();
		long totalTimeElapsed = 0;
		
		for(int i = 0; i < amount; i++) {
			
			// Times how long a single approach(check- in or out) took.
			long startTime = System.nanoTime();
			database.approach(1);
			long endTime = System.nanoTime();
			
			// Adds the times to a total later divided to get average.
			totalTimeElapsed += (endTime - startTime);
		}
		
		// Determining the total time the cycles took and the time elapsed using the database.
		long totalTime = System.nanoTime() - beginTime;
		totalTimeElapsed /= amount;
		
		// Printing results, please note that totalTime and totalTimeElapsed are converted to seconds,
		// since longs cannot contain fractured numbers they are converted to a double.
		System.out.println("-+- Results of Timed Cycle -+-");
		System.out.println("In total he database handled: " + amount + " Approach(es).");
		System.out.println("The entire process took: " + (double)totalTime/1000000000 + " Seconds.");
		System.out.println("On average each approach took: " + (double)totalTimeElapsed/1000000000 + " Seconds.");
		System.out.println("-+-                        -+-");
	}
	
	// Fills a list with data, random data that is.
	private static void fillWithData(List<String> list, int size) {
		for(int i = 0; i < size; i++) {
			list.add(Integer.toString((int)(Math.random() * 500 + 1)));
		}
	}
}
