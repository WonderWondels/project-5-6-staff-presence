package main;

import java.util.List;

public interface Database {
	public final static int SERVER_ERROR = -1;
	public final static int CHECKED_IN = 0;
	public final static int CHECKED_OUT = 1;
	
	public int approach(int uid);
	public int newEmployee(String employee);
	public Boolean isConnected();
	
	// Giving the file only works for the Flat File database and the whole thing was designed badly looking
	// back on my code. I'm too far in however to change it all up again.
	public List<String> getData(String file);
	public void writeData(List<String> list, String file);
	public void clear();
}
