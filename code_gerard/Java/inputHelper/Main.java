package inputHelper;

import java.awt.EventQueue;

import inputHelper.Panels.*;
import main.Database;
import relational.RelationalDB;

public class Main {
	
	private static int currentScreen = 0;
	static ProgramFrame background;
	static Database database;
	
	public static void main(String[] args) {
		Main machine = new Main();
	}
	
	public Main() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					background = new ProgramFrame();
					background.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		database = new RelationalDB();
		
		changeScreen(1, "");
		
		
		
	}
	
	
	public static void changeScreen(int t, String message) {
		currentScreen = t;
		System.out.println("Screen changed to: " + t);
		switch (t) {
		case 0:
			background.setPanel(new Wait(message));
			break;
		case 1:
			background.setPanel(new Home());
			break;
		case 2:
			background.setPanel(new NewEmployee(database));
		
	}
}

}
