package inputHelper;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import inputHelper.Panels.*;

import java.awt.Color;

public class ProgramFrame extends JFrame {

	private JPanel contentPane;
	private JPanel panel = new Wait("Het programma is bezig met opstarten.");

	public ProgramFrame() {
		getContentPane().setBackground(Color.LIGHT_GRAY);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setBounds(0, 0, 1920, 1080);
		this.setUndecorated(false);
		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.getContentPane().setLayout(null);
		this.getContentPane().add(panel);
	}
	
	public void setPanel(JPanel x) {
		this.panel.setVisible(false);
		this.panel = x;
		this.getContentPane().add(panel);
		this.panel.setVisible(true);
	}
	
	public static void showMessage(int x) {
		JOptionPane error = selectMessage(x);
	    final JDialog errorDialog = error.createDialog("Information");
	    errorDialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
	    new Thread(new Runnable() {
	      public void run() {
	        try {Thread.sleep(3000);} catch (InterruptedException e) {}
	        errorDialog.setVisible(false);
	      }
	    }).start();
	    errorDialog.setVisible(true);
	}
	
	private static JOptionPane selectMessage(int x) {
		switch(x) {
			case 0: return new JOptionPane("Handeling Succesvol Afgerond.", JOptionPane.INFORMATION_MESSAGE);
			case 1: return new JOptionPane("Verbinding met de server is niet mogelijk, het programma sluit af.", JOptionPane.WARNING_MESSAGE);
			case 2: return new JOptionPane("Vul AUB alle data in.", JOptionPane.INFORMATION_MESSAGE);
			default : return new JOptionPane("Er is iets fout gegaan.", JOptionPane.WARNING_MESSAGE);
		}
		
	}

}
