package inputHelper.Panels;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import inputHelper.HardwareControl;
import inputHelper.ProgramFrame;
import main.Database;

import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class NewEmployee extends JPanel {
	
	private Database database;
	
	Thread arduinoListener = new Thread() {
		public void run() {
			HardwareControl arduino = new HardwareControl();
			
			while(true) {
				System.out.println("Getting data.");
				String input = arduino.getInput();
				
				if(!input.equals("")) {
					System.out.println("Thread ended.");
					System.out.println(input);
					textUID.setText(input);
					break;
				}
				
				
			}
		}
	};
	private JTextField textName;
	private JTextField textFunctie;
	private JTextField textAfdeling;
	private JTextField textUID;

	/**
	 * Query needed as: naam, functie, afdeling, bhv, ehbo, uid
	 */
	public NewEmployee(Database db) {
		database = db;
		this.setBounds(150, 150, 1620, 930);
		this.setBackground(Color.LIGHT_GRAY);
		setLayout(null);
		
		final JLabel txtTitle = new JLabel("Nieuwe Medewerker");
		txtTitle.setBounds(610, 10, 400, 37);
		txtTitle.setFont(new Font("Tahoma", Font.BOLD, 30));
		txtTitle.setBackground(new Color(1.0f,1.0f,1.0f,0.0f));
		this.add(txtTitle);
		
		JTextArea txtMid = new JTextArea();
		txtMid.setBounds(620, 60, 449, 83);
		txtMid.setFont(new Font("Tahoma", Font.PLAIN, 30));
		txtMid.setBackground(new Color(1.0f,1.0f,1.0f,0.0f));
		txtMid.setText("Vul AUB alle velden in.");
		txtMid.setEditable(false);
		this.add(txtMid);
		
		textName = new JTextField();
		textName.setFont(new Font("Tahoma", Font.PLAIN, 18));
		textName.setBounds(381, 192, 247, 37);
		add(textName);
		textName.setColumns(10);
		
		JLabel lblNaam = new JLabel("Naam:");
		lblNaam.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNaam.setBounds(381, 170, 56, 16);
		add(lblNaam);
		
		textFunctie = new JTextField();
		textFunctie.setFont(new Font("Tahoma", Font.PLAIN, 18));
		textFunctie.setColumns(10);
		textFunctie.setBounds(381, 263, 247, 37);
		add(textFunctie);
		
		JLabel lblFunctie = new JLabel("Functie:");
		lblFunctie.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblFunctie.setBounds(381, 241, 83, 16);
		add(lblFunctie);
		
		textAfdeling = new JTextField();
		textAfdeling.setFont(new Font("Tahoma", Font.PLAIN, 18));
		textAfdeling.setColumns(10);
		textAfdeling.setBounds(381, 342, 247, 37);
		add(textAfdeling);
		
		JLabel lblAfdeling = new JLabel("Afdeling:");
		lblAfdeling.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblAfdeling.setBounds(381, 313, 83, 23);
		add(lblAfdeling);
		
		JLabel lblBhv = new JLabel("BHV:");
		lblBhv.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblBhv.setBounds(799, 167, 83, 23);
		add(lblBhv);
		
		JLabel lblEhbo = new JLabel("EHBO:");
		lblEhbo.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblEhbo.setBounds(799, 241, 83, 23);
		add(lblEhbo);
		
		JCheckBox checkBHV = new JCheckBox("");
		checkBHV.setBounds(799, 192, 25, 25);
		add(checkBHV);
		
		JCheckBox checkEHBO = new JCheckBox("");
		checkEHBO.setBounds(799, 271, 25, 25);
		add(checkEHBO);
		
		JLabel lblKaartId = new JLabel("Kaart ID:");
		lblKaartId.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblKaartId.setBounds(799, 318, 83, 23);
		add(lblKaartId);
		
		JButton btnZoeken = new JButton("Zoeken");
		btnZoeken.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				arduinoListener.start();
			}
		});
		btnZoeken.setBounds(881, 314, 97, 25);
		add(btnZoeken);
		
		textUID = new JTextField();
		textUID.setFont(new Font("Tahoma", Font.PLAIN, 18));
		textUID.setColumns(10);
		textUID.setBounds(799, 342, 247, 37);
		add(textUID);
		
		JButton btnNewButton = new JButton("Invoeren");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Write to DB
				String name = textName.getText();
				String functie = textFunctie.getText();
				String afdeling = textAfdeling.getText();
				String bhv = getValue(checkBHV);
				String ehbo = getValue(checkEHBO);
				String uid = textUID.getText();
				
				
				String employee = "";
				employee += "'" + name + "',";
				employee += "'" + functie + "',";
				employee += "'" + afdeling + "',";
				employee += "'" + bhv + "',";
				employee += "'" + ehbo + "',";
				employee += "'" + uid + "'";
				
				System.out.println(employee);
				ProgramFrame.showMessage(database.newEmployee(employee));
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 24));
		btnNewButton.setBounds(704, 481, 178, 48);
		add(btnNewButton);
		txtMid.setVisible(true);
	}
	
	private String getValue(JCheckBox x) {
		if(x.isSelected()) {
			return "1";
		}
		return "0";
	}
}
