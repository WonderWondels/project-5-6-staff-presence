package inputHelper.Panels;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.JTextField;

import inputHelper.Main;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Home extends JPanel {

	/**
	 * Create the panel.
	 */
	public Home() {
		this.setBounds(150, 150, 1620, 930);
		this.setBackground(Color.LIGHT_GRAY);
		setLayout(null);
		
		final JLabel txtTitle = new JLabel("Welkom,");
		txtTitle.setBounds(610, 10, 400, 37);
		txtTitle.setFont(new Font("Tahoma", Font.BOLD, 30));
		txtTitle.setBackground(new Color(1.0f,1.0f,1.0f,0.0f));
		this.add(txtTitle);
		
		JTextArea txtMid = new JTextArea();
		txtMid.setBounds(465, 60, 449, 68);
		txtMid.setFont(new Font("Tahoma", Font.PLAIN, 30));
		txtMid.setBackground(new Color(1.0f,1.0f,1.0f,0.0f));
		txtMid.setText("Kies een van de volgende opties.");
		txtMid.setEditable(false);
		this.add(txtMid);
		
		JButton btnNewButton = new JButton("Nieuwe Medewerker");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Main.changeScreen(2, "");
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnNewButton.setBounds(376, 141, 218, 48);
		add(btnNewButton);
		txtMid.setVisible(true);
	}

}
