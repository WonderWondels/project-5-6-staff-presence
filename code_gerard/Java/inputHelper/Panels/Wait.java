package inputHelper.Panels;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class Wait extends JPanel {

	/**
	 * Create the panel.
	 */
	public Wait(String text) {
		this.setBounds(150, 150, 1620, 930);
		this.setBackground(Color.LIGHT_GRAY);
		setLayout(null);
		
		final JLabel txtTitle = new JLabel("Even geduld AUB");
		txtTitle.setBounds(610, 10, 400, 37);
		txtTitle.setFont(new Font("Tahoma", Font.BOLD, 30));
		txtTitle.setBackground(new Color(1.0f,1.0f,1.0f,0.0f));
		this.add(txtTitle);
		
		JTextArea txtMid = new JTextArea();
		txtMid.setBounds(465, 60, 449, 179);
		txtMid.setFont(new Font("Tahoma", Font.PLAIN, 30));
		txtMid.setBackground(new Color(1.0f,1.0f,1.0f,0.0f));
		txtMid.setText(text);
		txtMid.setEditable(false);
		this.add(txtMid);
		txtMid.setVisible(true);
	}

}