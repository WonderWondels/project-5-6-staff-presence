package flatfile;


import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import main.Database;

public class FlatFileDB implements Database {
	
	private String fileName = "data.txt";
	
	@Override
	public int approach(int uid) {
		List<String> data = getData(fileName);
		Boolean checkin;
		int indexOfPerson = -1;
		String line = "";
		
		for(int i = 0; i < data.size(); i++) {
			line = data.get(i);
			
			if(line.contains(intToString(uid))) {
				indexOfPerson = i;
				break;
			}
		}
		
		if(indexOfPerson == -1) {
			return SERVER_ERROR;
		}
		
		String parts[] = line.split("-");
		
		if(parts[2].equals("1")) {
			checkin = false;
			parts[2] = "0";
		} else {
			checkin = true;
			parts[2] = "1";
		}
		
		data.set(indexOfPerson, parts[0]+"-"+parts[1]+"-"+parts[2]);
		
		writeData(data, fileName);
		
		if(checkin) {
			return CHECKED_IN;
		} else {
			return CHECKED_OUT;
		}
	}

	@Override
	public void clear() {
		try {
			PrintWriter writer = new PrintWriter(new File(fileName));
			writer.print("");
			writer.close();
		} catch (Exception e) {
			
		}
		
	}

	@Override
	public Boolean isConnected() {
		// Flat file DB is always connected
		return true;
	}
	
	private String intToString(int x) {
		String converted = Integer.toString(x);
		while(!(converted.length() == 4)) {
			converted = "0" + converted;
		}
		return converted;
	}
	
	public List<String> getData(String file){
		// Setting up the scanner and a list to fill.
		Scanner db = null;
		try {db = new Scanner(new File(file));} catch (Exception e) {}
		List<String> list = new ArrayList<String>();
		
		
		// Scanner might throw NoSuchElementException when no elements are left.
		try {
			String line;
			while((line = db.nextLine()) != null) {
				System.out.println(line);
				list.add(line);
			}
		} catch (Exception e) {}
		
		return list;
	}
	
	public void writeData(List<String> list, String file) {
		try {
			PrintWriter writer = new PrintWriter(new File(file));
			writer.print("");
			
			for(int i = 0; i < list.size(); i++) {
				writer.println(list.get(i));
			}
			
			writer.close();
		} catch (Exception e) {
			
		}
		
	}

	@Override
	public int newEmployee(String employee) {
		
		return 0;
	}
	
}