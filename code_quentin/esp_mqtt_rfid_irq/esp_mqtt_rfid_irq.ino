/***************************************************
NodeMCU
****************************************************/
#include <ESP8266WiFi.h>
#include "Adafruit_MQTT.h"
#include "Adafruit_MQTT_Client.h"
#include <SPI.h>
#include <MFRC522.h>

//because pins on the nodemcu are f'ed up
// static const uint8_t D0 = 16;
// static const uint8_t D1 = 5;
// static const uint8_t D2 = 4;
// static const uint8_t D3 = 0;
// static const uint8_t D4 = 2;
// static const uint8_t D5 = 14;
// static const uint8_t D6 = 12;
// static const uint8_t D7 = 13;
// static const uint8_t D8 = 15;
// static const uint8_t D9 = 3;
// static const uint8_t D10= 1;

/**************************** Other setup *****************/

//Pin definitions
#define red 15
#define green 2

//Used for intervals without delay()
int pinState = LOW;
unsigned long previousMillis = 1000;
unsigned long previousMillis2nd = 1000;
unsigned long previousMillisMqtt = 1000;
unsigned long currentMillis = millis();

/**************************** RFID setup *****************
Pin mapping:
----------------
RC522 | NodeMCU
----------------
SDA   | D2  (configurable, see below)
SCK   | D5
MOSI  | D7
MISO  | D6
IRQ   | D3  (configurable, see below)
GND   | GND
RST   | D1  (configurable, see below)
3.3V  | 3V3
*/

#define RST_PIN 5   // D1 (GPIO05) Configurable, see typical pin layout above
#define SS_PIN  4   // D2 (GPIO04) Configurable, see typical pin layout above
#define IRQ_PIN 0   // D3 (GPIO00) Configurable, see typical pin layout above

MFRC522 rfid(SS_PIN, RST_PIN); // Instance of the RFID

//MFRC522::MIFARE_Key key;
String content= "";   //String of UID will be saved here
volatile boolean newInterrupt = false;
/*A variable should be declared volatile whenever
its value can be changed by something beyond the control of the code section in which
it appears, such as a concurrently executing thread. In the Arduino, the only place
that this is likely to occur is in sections of code associated with interrupts, called
an interrupt service routine.*/
byte regVal = 0x7F;
void activateRec(MFRC522 rfid);
void clearInt(MFRC522 rfid);

// Init array that will store new NUID
byte nuidPICC[4];

/************************* WiFi Access Point *********************************/
// Hotstop Q
// #define WLAN_SSID       "NetworQ"
// #define WLAN_PASS       "wifififi"

#define WLAN_SSID       "WITH"
#define WLAN_PASS       "wonderwondels"

//Hotspot P
// #define WLAN_SSID       "SamsungS8"
// #define WLAN_PASS       "yiwk1234"

/************************* MQTT Setup *********************************/
// CloudMQTT server creds
// #define MQTT_SERVER     "m21.cloudmqtt.com"
// //#define MQTT_SERVER     "m21.cloudmqtt.commm"
// #define MQTT_PORT       19731
// #define MQTT_USERNAME   "kczoznip"
// #define MQTT_PASSWORD   "rUq7RlG5xEQV"

// Raspberry Pi server creds
#define MQTT_SERVER     "192.168.0.103"
#define MQTT_PORT       1883
#define MQTT_USERNAME   "esp"
#define MQTT_PASSWORD   "wonderwondels"

// Create an ESP8266 WiFiClient class to connect to the MQTT server.
WiFiClient client;
// Setup the MQTT client class by passing in the WiFi client and MQTT server and login details.
Adafruit_MQTT_Client mqtt(&client, MQTT_SERVER, MQTT_PORT, MQTT_USERNAME, MQTT_PASSWORD);
// Setup a feed called 'uid' for publishing.
Adafruit_MQTT_Publish uid = Adafruit_MQTT_Publish(&mqtt, "/esp/uid");
// Setup a feed called 'piMasterSays' for subscribing to changes.
Adafruit_MQTT_Subscribe piMasterSays = Adafruit_MQTT_Subscribe(&mqtt,"/pi/access");

/************************* Setup/Loop *********************************/

void setup() {
    Serial.begin(115200);
    delay(500);
    previousMillis=millis();

    /******** LED setup********/
    pinMode(red, OUTPUT);
    pinMode(green, OUTPUT);
    digitalWrite(red, LOW);
    digitalWrite(green, LOW);

    /********** Wifi setup*******/
    Serial.println(); Serial.println();
    Serial.print("Connecting to ");
    Serial.println(WLAN_SSID);
    WiFi.mode(WIFI_STA);
    WiFi.begin(WLAN_SSID, WLAN_PASS);
    while (WiFi.status() != WL_CONNECTED) {
        ledInterval(red, 500);
        //Serial.print(".");
        delay(2);
    }
    Serial.println();
    Serial.println("WiFi connected");
    Serial.println("IP address: "); Serial.println(WiFi.localIP());
    digitalWrite(red, LOW);
    digitalWrite(green, LOW);
    delay(100); //necessary, otherwise MQTT setup fails

    /******** RFID setup********/
    Serial.println("****Setting up RFID****");
    pinMode(IRQ_PIN, INPUT_PULLUP);
    attachInterrupt(digitalPinToInterrupt(IRQ_PIN), raiseInterruptFlag, FALLING); //run raiseInterruptFlag() when interrupt goes from high to low
    Serial.println("irq pin set");
    SPI.begin(); // Init SPI bus
    Serial.println("SPI started");
    delay(500);
    rfid.PCD_Init(); // Init MFRC522
    Serial.println("rfid init");
    delay(500);
    //Allow the ... irq to be propagated to the IRQ pin
    regVal = 0xA0; //rx irq
    Serial.println("setting reg value");
    rfid.PCD_WriteRegister(rfid.ComIEnReg, regVal);
    //printHex(key.keyByte, MFRC522::MF_KEY_SIZE);
    Serial.println("****RFID done ****");
    delay(100);

    /********** MQTT setup*******/
    // Setup MQTT subscription for piMasterSays feed.
    mqtt.subscribe(&piMasterSays);
    MQTT_connect(5);
    Serial.println("------Setup complete!-------");
}

void loop() {
    // Ensure the connection to the MQTT server is alive (this will make the first
    // connection and automatically reconnect when disconnected).  See the MQTT_connect
    if(!(mqtt.connected())){
        Serial.println("Lost connection. Reconnecting...");
        MQTT_connect(1);
    }

    // this is our 'wait for incoming subscription packets' busy subloop
    // try to spend your time here
    // Here its read the subscription
    //ping the server every 4 mins to keep alive
    pingMqttServer(240000);

    Adafruit_MQTT_Subscribe *subscription;
    while ((subscription = mqtt.readSubscription())) {
        Serial.print("New msg in sub. ");
        if (subscription == &piMasterSays) {
            char *message = (char *)piMasterSays.lastread;
            Serial.print(F("Got: "));
            Serial.println(message);
                //0 - denied
                //1 - granted/check in
                //2 - check out
            if (strncmp(message, "1", 1) == 0) {
                accessGranted();
            }
            else if (strncmp(message, "2", 1) == 0) {
                checkOut();
            }
            else if (strncmp(message, "0", 1) == 0) {
                accessDenied();
            }
            else{
                cardUnknown();
            }
        }
    }

    if (newInterrupt) { //new read interrupt
        Serial.print("Interrupted");
        rfid.PICC_ReadCardSerial(); //read the tag data
        Serial.println();
        rfidInterrupt();
        clearInt(rfid);
        rfid.PICC_HaltA();
        newInterrupt = false; //reset interrupt flag
    }
    // The receiving block needs regular retriggering (tell the tag it should transmit??)
    // (rfid.PCD_WriteRegister(rfid.FIFODataReg,rfid.PICC_CMD_REQA);)
    activateRec(rfid);
    delay(50); //delay's feed the watchdog
}

/************************* MQTT functions *********************************/

void MQTT_connect(int attemptLimit) {
    // Function to connect and reconnect as necessary to the MQTT server.
    float safetyNetDelay = 5000; //safety net delay for connection reattempt
    digitalWrite(red, HIGH);
    digitalWrite(green, HIGH);
    Serial.print("Connecting to MQTT... ");
    mqtt.connect();
    Serial.print("Connect attempted... ");
    // Stop if already connected.
    if (mqtt.connected()) {
        digitalWrite(red, HIGH);
        digitalWrite(green, HIGH);
        Serial.println("MQTT Connected!");
        delay(700);
        digitalWrite(red, LOW);
        digitalWrite(green, LOW);
        return;
    }
    int8_t status;
    int delayRemaining;

    while ((status = mqtt.connected()) == 0) { // connected will return 0 for not connected
        if (attemptLimit == 0) {
            // basically die and wait for WDT to reset ESP
            Serial.println("Connection failed. Restarting...");
            while (1);
        }
        Serial.println(mqtt.connectErrorString(status));
        Serial.print("Retrying MQTT connection in ");
        delayRemaining = (safetyNetDelay - (currentMillis - previousMillis)) / 1000;
        Serial.print(delayRemaining);
        Serial.println(" second(s)...");
        currentMillis = millis();
        if ((currentMillis - previousMillis) >= safetyNetDelay) {
            Serial.print("d conn:");
            Serial.println((currentMillis - previousMillis));
            previousMillis = currentMillis;
            Serial.println("Safety net delay");
            Serial.println("Attempts left: " + attemptLimit);
            Serial.println(attemptLimit);
            attemptLimit--;
            currentMillis = millis();
            mqtt.connect();
        }
        ledInterval(green, 200);
        mqtt.disconnect();
        //Serial.println("Turning green led on");
        //delay(100);
        status = mqtt.connected();
    }
    delay(10);
}

void pingMqttServer(int interval){
    //The server needs to be pinged every 5 mins to keep connection alive.
    currentMillis = millis();
    if ((currentMillis - previousMillis) >= interval) {
        previousMillis = currentMillis;
        Serial.println("ping");
        if(! mqtt.ping()) {
            mqtt.disconnect();
        }
    }
}

/************************* RFID functions *********************************/
void raiseInterruptFlag() {
    /* MFRC522 interrupt serving routine */
    Serial.print("Interrupt flag");
    newInterrupt = true;
}

void rfidInterrupt() {
    //code to run when interrupt is triggered
    //Show UID on serial monitor
    Serial.print("UID tag :");
    byte letter;			//temp placeholder for ascii letter
    for (byte i = 0; i < rfid.uid.size; i++) //convert read byte into a hex value and add to string container
    {
        Serial.print(rfid.uid.uidByte[i] < 0x10 ? "0" : "");
        Serial.print(rfid.uid.uidByte[i], HEX);
        content.concat(String(rfid.uid.uidByte[i] < 0x10 ? "0" : ""));
        content.concat(String(rfid.uid.uidByte[i], HEX));
    }
    Serial.println();
    Serial.print("Message : ");
    content.toUpperCase();
    uid.publish(content.c_str());	//publish UID string to MQTT
    Serial.print("Sent via MQTT: ");
    Serial.println(content);
    content=""; //empty string again
}

void activateRec(MFRC522 mfrc522) {
    /* This function sends the needed commands to MFRC522 to activate the reception of data */
    mfrc522.PCD_WriteRegister(mfrc522.FIFODataReg, mfrc522.PICC_CMD_REQA);
    mfrc522.PCD_WriteRegister(mfrc522.CommandReg, mfrc522.PCD_Transceive);
    mfrc522.PCD_WriteRegister(mfrc522.BitFramingReg, 0x87);
}

void clearInt(MFRC522 mfrc522) {
    /* The function to clear the pending interrupt bits after interrupt serving routine */
    mfrc522.PCD_WriteRegister(mfrc522.ComIrqReg, 0x7F);
}

/************************* Other functions *********************************/

void ledInterval(int pin, int interval){
    currentMillis = millis();
    if ((currentMillis - previousMillis) >= interval) {
        // save the last time you blinked the LED
        previousMillis = currentMillis;
        Serial.println("led flash");
        // if the LED is off turn it on and vice-versa:
        if (pinState == LOW) {
            pinState = HIGH;
        }
        else {
            pinState = LOW;
        }
        // set the LED with the ledState of the variable:
        digitalWrite(pin, pinState);
    }
}

void accessDenied(){
    digitalWrite(red, HIGH);
    delay(1000);
    digitalWrite(red, LOW);
}

void accessGranted(){
    digitalWrite(green, HIGH);
    delay(700);
    digitalWrite(green, LOW);
}

void checkOut(){
    digitalWrite(green, HIGH);
    delay(100);
    digitalWrite(green, LOW);
    delay(300);
    digitalWrite(green, HIGH);
    delay(100);
    digitalWrite(green, LOW);
}

void cardUnknown(){
    digitalWrite(green, HIGH);
    digitalWrite(red, HIGH);
    delay(200);
    digitalWrite(green, LOW);
    digitalWrite(red, LOW);
    delay(500);
    digitalWrite(green, HIGH);
    digitalWrite(red, HIGH);
    delay(200);
    digitalWrite(green, LOW);
    digitalWrite(red, LOW);
    delay(500);
    digitalWrite(green, HIGH);
    digitalWrite(red, HIGH);
    delay(200);
    digitalWrite(green, LOW);
    digitalWrite(red, LOW);
}
