/*************************************************** 
  NodeMCU
****************************************************/ 
#include <ESP8266WiFi.h> 
#include "Adafruit_MQTT.h" 
#include "Adafruit_MQTT_Client.h" 
#include <SPI.h>
#include <MFRC522.h>

//because pins on the nodemcu are f'ed up
// static const uint8_t D0   = 16;
// static const uint8_t D1   = 5;
// static const uint8_t D2   = 4;
// static const uint8_t D3   = 0;
// static const uint8_t D4   = 2;
// static const uint8_t D5   = 14;
// static const uint8_t D6   = 12;
// static const uint8_t D7   = 13;
// static const uint8_t D8   = 15;
// static const uint8_t D9   = 3;
// static const uint8_t D10  = 1;



/**************************** RFID setup *****************/

// #define RST_PIN 5    // NodeMCU D1 (GPIO05)
// #define SS_PIN 4     // NodeMCU D2 (GPIO04)
// #define IRQ_PIN 2	 // Node MCU D4

constexpr uint8_t RST_PIN 	= 5;     // Configurable, see typical pin layout above
constexpr uint8_t SS_PIN 	= 4;     // Configurable, see typical pin layout above
constexpr uint8_t IRQ_PIN 	= 0;	 //D3


MFRC522 rfid(SS_PIN, RST_PIN); // Instance of the class

MFRC522::MIFARE_Key key; 

// Init array that will store new NUID 
byte nuidPICC[4];

/************************* WiFi Access Point *********************************/ 
#define WLAN_SSID       "NetworQ" 
#define WLAN_PASS       "wifififi" 
#define MQTT_SERVER     "m21.cloudmqtt.com"
#define MQTT_PORT       19731                    
#define MQTT_USERNAME   "kczoznip" 
#define MQTT_PASSWORD   "rUq7RlG5xEQV" 
//mosquitto_sub -h "yourlocalhost" -t "#" to see the messages in the channel

/************ Global State ******************/ 
// Create an ESP8266 WiFiClient class to connect to the MQTT server. 
WiFiClient client; 
// Setup the MQTT client class by passing in the WiFi client and MQTT server and login details. 
Adafruit_MQTT_Client mqtt(&client, MQTT_SERVER, MQTT_PORT, MQTT_USERNAME, MQTT_PASSWORD); 

/****************************** Feeds ***************************************/ 
// Setup a feed called 'uid' for publishing. 
// Notice MQTT paths for AIO follow the form: <username>/feeds/<feedname> 
Adafruit_MQTT_Publish uid = Adafruit_MQTT_Publish(&mqtt, MQTT_USERNAME "/esp/uid"); 
// Setup a feed called 'esp8266_led' for subscribing to changes. 
//Adafruit_MQTT_Subscribe esp8266_led = Adafruit_MQTT_Subscribe(&mqtt, MQTT_USERNAME "/leds/esp8266"); 
/*************************** Sketch Code ************************************/ 

uint32_t x=0; 

void MQTT_connect(); 
void setup() { 
  	Serial.begin(115200); 
  	delay(500);
   	/* RFID setup*/
  	Serial.println("****Setting up RFID****");
  	SPI.begin(); // Init SPI bus
  	Serial.println("spi started");
  	delay(500);
  	rfid.PCD_Init(); // Init MFRC522 
  	Serial.println("rfid init");
  	 for (byte i = 0; i < 6; i++) {
      //apparently this needed to init spi
  	 	key.keyByte[i] = 0xFF;
  	 }
  	delay(500); 
  	Serial.println("****RFID done ****");
   
  	Serial.println(F("RPi-ESP-MQTT")); 
 	// Connect to WiFi access point. 
 	/* Wifi setup*/
  	Serial.println(); Serial.println(); 
  	Serial.print("Connecting to "); 
  	Serial.println(WLAN_SSID); 
  	WiFi.begin(WLAN_SSID, WLAN_PASS); 
  	while (WiFi.status() != WL_CONNECTED) { 
  		delay(500); 
  		Serial.print("."); 
  	} 
  	Serial.println(); 
  	Serial.println("WiFi connected"); 
  	Serial.println("IP address: "); Serial.println(WiFi.localIP()); 
 	// Setup MQTT subscription for esp8266_led feed. 
  	//mqtt.subscribe(&esp8266_led);
  } 
 
  void loop() {
	 // Ensure the connection to the MQTT server is alive (this will make the first 
	 // connection and automatically reconnect when disconnected).  See the MQTT_connect 
  	MQTT_connect(); 
	// this is our 'wait for incoming subscription packets' busy subloop 
	// try to spend your time here 
	// Here its read the subscription 
	// 	Adafruit_MQTT_Subscribe *subscription;

  // Look for new cards
  if ( ! rfid.PICC_IsNewCardPresent())
    return;

  // Verify if the NUID has been readed
  if ( ! rfid.PICC_ReadCardSerial())
    return;

  Serial.print(F("PICC type: "));
  MFRC522::PICC_Type piccType = rfid.PICC_GetType(rfid.uid.sak);
  Serial.println(rfid.PICC_GetTypeName(piccType));

  // Check is the PICC of Classic MIFARE type
  if (piccType != MFRC522::PICC_TYPE_MIFARE_MINI &&  
    piccType != MFRC522::PICC_TYPE_MIFARE_1K &&
    piccType != MFRC522::PICC_TYPE_MIFARE_4K) {
    Serial.println(F("Your tag is not of type MIFARE Classic."));
    return;
  }

  if (rfid.uid.uidByte[0] != nuidPICC[0] || 
    rfid.uid.uidByte[1] != nuidPICC[1] || 
    rfid.uid.uidByte[2] != nuidPICC[2] || 
    rfid.uid.uidByte[3] != nuidPICC[3] ) {
    Serial.println(F("A new card has been detected."));

    // Store NUID into nuidPICC array
    for (byte i = 0; i < 4; i++) {
      nuidPICC[i] = rfid.uid.uidByte[i];
    }
   
    Serial.print("UID tag :");
    String content= "";   //make a string container
    byte letter;      //temp placeholder for ascii letter
    for (byte i = 0; i < rfid.uid.size; i++) //convert read byte into a hex value and add to string container
    {
      Serial.print(rfid.uid.uidByte[i] < 0x10 ? " 0" : " ");
      Serial.print(rfid.uid.uidByte[i], HEX);
      content.concat(String(rfid.uid.uidByte[i] < 0x10 ? " 0" : " "));
      content.concat(String(rfid.uid.uidByte[i], HEX));
    }
    Serial.println();
    Serial.print("Message : ");
    content.toUpperCase();
    Serial.print("Sending via MQTT");
    Serial.println(content);
    uid.publish(content.c_str()); //publish UID string to MQTT
  }
  else Serial.println(F("Card read previously."));

  // Halt PICC
  rfid.PICC_HaltA();

  // Stop encryption on PCD
  rfid.PCD_StopCrypto1();
  delay(100);
  }
	
  //code to run when interrupt is triggered
	// Function to connect and reconnect as necessary to the MQTT server. 
  void MQTT_connect() { 
  	int8_t ret; 
 	// Stop if already connected. 
  	if (mqtt.connected()) { 
  		return; 
  	} 
  	Serial.print("Connecting to MQTT... "); 
  	uint8_t retries = 3; 
 	while ((ret = mqtt.connect()) != 0) { // connect will return 0 for connected 
 		Serial.println(mqtt.connectErrorString(ret)); 
 		Serial.println("Retrying MQTT connection in 5 seconds..."); 
 		mqtt.disconnect(); 
     	delay(5000);  // wait 5 seconds 
     	retries--; 
      	if (retries == 0) { 
        // basically die and wait for WDT to reset me 
      		while (1); 
    	} 
  	} 
  Serial.println("MQTT Connected!"); 
}
/**
 * Helper routine to dump a byte array as dec values to Serial.
 */
// void printDec(byte *buffer, byte bufferSize) {
// 	for (byte i = 0; i < bufferSize; i++) {
// 		Serial.print(buffer[i] < 0x10 ? " 0" : " ");
// 		Serial.print(buffer[i], DEC);
// 	}
// }
 void printHex(byte *buffer, byte bufferSize) {
 	for (byte i = 0; i < bufferSize; i++) {
 		Serial.print(buffer[i] < 0x10 ? " 0" : " ");
 		Serial.print(buffer[i], HEX);
 	}
 }

 /**
   Helper routine to dump a byte array as hex values to Serial.
*/
void dump_byte_array(byte *buffer, byte bufferSize) {
  for (byte i = 0; i < bufferSize; i++) {
    Serial.print(buffer[i] < 0x10 ? " 0" : " ");
    Serial.print(buffer[i], HEX);
  }
}

