#include <ESP8266WiFi.h>
#include <PubSubClient.h>

const char* ssid = "Tesla IoT";	              		// wifi ssid
const char* password =	"fsL6HgjN";			// wifi passwd
const char* mqttServer = "145.24.238.110";	  	// IP adress Pi
const int mqttPort = 1883;				// open port
const char* mqttUser = "pi";		    		// mqtt user
const char* mqttPassword = "BoterKaasenKip2610";	// mqtt passwd

/* Adding WiFi client, ESP */
/* and adding PubSubClient, ESP */
WiFiClient espClient;						
PubSubClient client(espClient);					

void setup() {
	/* starts serial comminucation */
	Serial.begin(115200);
	
	/*
	 * tries to connect to wifi, using a while loop which keeps
	 * checking if the status is connected... if so he breaks
	 * out of the while loop
	 */	 
	WiFi.begin(ssid, password);
	while (WiFi.status() != WL_CONNECTED) {
	  delay(500);
	  Serial.println("Connecting to WiFi..");
	}
	Serial.println("Connected to the WiFi network");

	/*
	 * sets server and callback, the server will ofcourse be at the
	 * ip-adress of the pi and the port aswell; callback tries
	 * to send the message to the right topic 
	 */
	client.setServer(mqttServer, mqttPort);
	client.setCallback(callback);

	/*
	 * Loops through as long as the client is not connected, this piece of
	 * code literally tries to connect to the client.
	 */
	while (!client.connected()) {
		Serial.println("Connecting to MQTT...");
	  	if (client.connect("ESP8266Client", mqttUser, mqttPassword )) {
			Serial.println("connected");
		} else {
	  	  	Serial.print("failed with state ");
	  	  	Serial.print(client.state());
	  	  	delay(2000);
	  	}
	}

       /*
        * //In case there is just a one time publish needed:
        * client.publish("esp8266", "Hello Raspberry Pi");
        * client.subscribe("esp8266");
        */


}

void callback(char* topic, byte* payload, unsigned int length) {

	Serial.print("Message arrived in topic: ");
	Serial.println(topic);

	Serial.print("Message:");
	/* Switch on the LED if an 1 was received as first character */
	if ((char)payload[0] == '1') {
	       /*
		* Turn the LED on (Note that LOW is the voltage level
		* but actually the LED is on; this is because
		* it is active low on the ESP-01)
		*/
		digitalWrite(BUILTIN_LED, LOW);   
	} else {
	  	digitalWrite(BUILTIN_LED, HIGH);  	
	}	
	for (int i = 0; i < length; i++) {
	  	Serial.print((char)payload[i]);
	}
	Serial.println();
	Serial.println("-----------------------");

}

void loop() {
	/* This just causes the client to keep sending Hello! */
	client.publish("esp8266", "Hello Raspberry Pi");
	client.subscribe("esp8266");
	delay(300);
	client.loop();
}
