/*************************************************** 
  NodeMCU
****************************************************/ 
#include <ESP8266WiFi.h> 
#include "Adafruit_MQTT.h" 
#include "Adafruit_MQTT_Client.h" 
#include <SPI.h>
#include <MFRC522.h>

//because pins on the nodemcu are f'ed up
// static const uint8_t D0 = 16;
// static const uint8_t D1 = 5;
// static const uint8_t D2 = 4;
// static const uint8_t D3 = 0;
// static const uint8_t D4 = 2;
// static const uint8_t D5 = 14;
// static const uint8_t D6 = 12;
// static const uint8_t D7 = 13;
// static const uint8_t D8 = 15;
// static const uint8_t D9 = 3;
// static const uint8_t D10= 1;

/**************************** RFID setup *****************/
/*
    RC522 | NodeMCU
    ----------------
    SDA   | D2  (configurable, see below)
    SCK   | D5
    MOSI  | D7
    MISO  | D6
    IRQ   | D3  (configurable, see below)
    GND   | GND
    RST   | D1  (configurable, see below)
    3.3V  | 3V3

*/

#define RST_PIN 5   // D1 (GPIO05) Configurable, see typical pin layout above
#define SS_PIN  4   // D2 (GPIO04) Configurable, see typical pin layout above
#define IRQ_PIN 0   // D3 (GPIO00) Configurable, see typical pin layout above

MFRC522 rfid(SS_PIN, RST_PIN); // Instance of the RFID

//MFRC522::MIFARE_Key key; 
String content= "";   //String of UID will be saved here
volatile boolean newInterrupt = false;
/*A variable should be declared volatile whenever 
its value can be changed by something beyond the control of the code section in which
it appears, such as a concurrently executing thread. In the Arduino, the only place
that this is likely to occur is in sections of code associated with interrupts, called
an interrupt service routine.*/
byte regVal = 0x7F;
void activateRec(MFRC522 rfid);
void clearInt(MFRC522 rfid);

// Init array that will store new NUID 
byte nuidPICC[4];

/************************* WiFi Access Point *********************************/ 
#define WLAN_SSID       "NetworQ" 
#define WLAN_PASS       "wifififi" 
#define MQTT_SERVER     "m21.cloudmqtt.com"
#define MQTT_PORT       19731                    
#define MQTT_USERNAME   "kczoznip" 
#define MQTT_PASSWORD   "rUq7RlG5xEQV" 
//mosquitto_sub -h "yourlocalhost" -t "#" to see the messages in the channel

/************ Global State ******************/ 
// Create an ESP8266 WiFiClient class to connect to the MQTT server. 
WiFiClient client; 
// Setup the MQTT client class by passing in the WiFi client and MQTT server and login details. 
Adafruit_MQTT_Client mqtt(&client, MQTT_SERVER, MQTT_PORT, MQTT_USERNAME, MQTT_PASSWORD); 

/****************************** Feeds ***************************************/ 
// Setup a feed called 'uid' for publishing. 
// Notice MQTT paths for AIO follow the form: <username>/feeds/<feedname> 
Adafruit_MQTT_Publish uid = Adafruit_MQTT_Publish(&mqtt, MQTT_USERNAME "/esp/uid"); 
// Setup a feed called 'esp8266_led' for subscribing to changes. 
//Adafruit_MQTT_Subscribe esp8266_led = Adafruit_MQTT_Subscribe(&mqtt, MQTT_USERNAME "/leds/esp8266"); 
/*************************** Sketch Code ************************************/ 

//uint32_t x=0; 

  void MQTT_connect(); 
  void setup() { 
  	Serial.begin(115200); 
  	delay(500);
   	/******** RFID setup********/
  	Serial.println("****Setting up RFID****");
  	pinMode(IRQ_PIN, INPUT_PULLUP);
    attachInterrupt(digitalPinToInterrupt(IRQ_PIN), readCard, FALLING); //run readCard() when interrupt goes from high to low
    Serial.println("irq pin set");
  	SPI.begin(); // Init SPI bus
  	Serial.println("SPI started");
  	delay(500);
  	rfid.PCD_Init(); // Init MFRC522 
  	Serial.println("rfid init");
  	delay(500);
     //Allow the ... irq to be propagated to the IRQ pin
    regVal = 0xA0; //rx irq
    Serial.println("setting reg value");
    rfid.PCD_WriteRegister(rfid.ComIEnReg, regVal);
    //printHex(key.keyByte, MFRC522::MF_KEY_SIZE);
    Serial.println("****RFID done ****");
 	// Connect to WiFi access point. 
 	/* Wifi setup*/
    Serial.println(); Serial.println(); 
    Serial.print("Connecting to "); 
    Serial.println(WLAN_SSID); 
    WiFi.begin(WLAN_SSID, WLAN_PASS); 
    while (WiFi.status() != WL_CONNECTED) { 
      delay(500); 
      Serial.print("."); 
    } 
    Serial.println(); 
    Serial.println("WiFi connected"); 
    Serial.println("IP address: "); Serial.println(WiFi.localIP()); 
 	// Setup MQTT subscription for esp8266_led feed. 
  	//mqtt.subscribe(&esp8266_led);

  } 

  void loop() {
	 // Ensure the connection to the MQTT server is alive (this will make the first 
	 // connection and automatically reconnect when disconnected).  See the MQTT_connect 
    MQTT_connect(); 
	// this is our 'wait for incoming subscription packets' busy subloop 
	// try to spend your time here 
	// Here its read the subscription 
	// 	Adafruit_MQTT_Subscribe *subscription;
  if (newInterrupt) { //new read interrupt
    Serial.print("Interrupt. ");
    rfid.PICC_ReadCardSerial(); //read the tag data
    Serial.println();
    rfidInterrupt();
    clearInt(rfid);
    rfid.PICC_HaltA();
    newInterrupt = false;
  } 
 // The receiving block needs regular retriggering (tell the tag it should transmit??)
  // (rfid.PCD_WriteRegister(rfid.FIFODataReg,rfid.PICC_CMD_REQA);)
  activateRec(rfid);
  delay(50); //delay's feed the watchdog
}
  /*
   MFRC522 interrupt serving routine
  */
void readCard() {
  newInterrupt = true;
}	
  //code to run when interrupt is triggered
void rfidInterrupt() {
  	//Show UID on serial monitor
 Serial.print("UID tag :");
  	byte letter;			//temp placeholder for ascii letter
  	for (byte i = 0; i < rfid.uid.size; i++) //convert read byte into a hex value and add to string container
  	{
  		Serial.print(rfid.uid.uidByte[i] < 0x10 ? " 0" : " ");
  		Serial.print(rfid.uid.uidByte[i], HEX);
  		content.concat(String(rfid.uid.uidByte[i] < 0x10 ? " 0" : " "));
content.concat(String(rfid.uid.uidByte[i], HEX));
}
Serial.println();
Serial.print("Message : ");
content.toUpperCase();
  	uid.publish(content.c_str());	//publish UID string to MQTT
    Serial.print("Sent via MQTT: ");
    Serial.println(content);
    content=""; //empty string again
  }
	// Function to connect and reconnect as necessary to the MQTT server. 
void MQTT_connect() { 
	int8_t status; 
	// Stop if already connected. 
	if (mqtt.connected()) { 
		return; 
	} 
	Serial.print("Connecting to MQTT... "); 
	uint8_t retries = 3; 
 	while ((status = mqtt.connect()) != 0) { // connect will return 0 for connected 
 		Serial.println(mqtt.connectErrorString(status)); 
 		Serial.println("Retrying MQTT connection in 5 seconds..."); 
 		mqtt.disconnect(); 
     	delay(5000);  // wait 5 seconds 
     	retries--; 
       if (retries == 0) { 
        // basically die and wait for WDT to reset ESP
        while (1); 
      } 
  } 
Serial.println("MQTT Connected!"); 
}
/*
   This function sends the needed commands to MFRC522 to activate the reception of data
*/
void activateRec(MFRC522 mfrc522) {
  mfrc522.PCD_WriteRegister(mfrc522.FIFODataReg, mfrc522.PICC_CMD_REQA);
  mfrc522.PCD_WriteRegister(mfrc522.CommandReg, mfrc522.PCD_Transceive);
  mfrc522.PCD_WriteRegister(mfrc522.BitFramingReg, 0x87);
}
/*
   The function to clear the pending interrupt bits after interrupt serving routine
*/
void clearInt(MFRC522 mfrc522) {
  mfrc522.PCD_WriteRegister(mfrc522.ComIrqReg, 0x7F);
}

